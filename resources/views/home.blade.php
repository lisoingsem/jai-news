@extends('layouts.master')

@section('content')
<div class="container mt-2 p-0">
      <div class="row  features ">
            <!-- left side  -->
            <div class="col-md-8 left-features">

                  <!-- ads  -->
                  <div class="row ads-block d-sm-block d-md-none mb-3">
                        <div class="col-12">
                              @include('ads-block-one')
                        </div>
                  </div>

                  <!-- biggest news box -->
                  <a class="card bg-dark text-white biggest-features-news" href="fake.html">
                        <img src="assets/image/camera_lense_0.jpeg" class="card-img" alt="assets/image/camera_lense_0.jpeg">
                        <div class="card-overlay">
                              <h5 class="card-title">ឧកញ៉ាបណ្ឌិត គីម ហ៊ាង នឹងឡើងបកស្រាយពី
                                    “ដំណោះស្រាយវិស័យអចលនទ្រព្យនៅកម្ពុជា” នៅព្រីមៀរសែនសុខ ដោយឥតគិតថ្លៃ</h5>
                              <p class="card-text">
                                    <span>August 29, 2023</span>
                              </p>
                              <p class="card-text justify-content-between d-flex">
                                    <span>អានអត្ថបទបន្ថែម</span>
                                    <span><i class="fa-solid fa-circle-arrow-right"></i></span>
                              </p>
                        </div>
                  </a>

                  <!-- new feed -->
                  <div class="row new-feed mt-4">
                        @include('news-feed')
                  </div>

                  <!-- ads -->
                  <div class="row ads-block d-sm-block d-md-none">
                        <div class="col-12">
                              @include('ads-block-two')
                        </div>
                  </div>

                  <!-- category -->
                  @include('category-by-block')



            </div>
            <!-- end left side  -->



            <!-- right side  -->
            <div class="col-md-4 right-block d-sm-none d-md-block  d-none">

                  <div class="row news-ads">
                        <div class="col-12">
                              <!-- ads  -->
                              <div class="row ads-block d-sm-none d-none d-md-block">
                                    <div class="col-12">
                                          @include('ads-block-one')
                                    </div>
                              </div>

                              <!-- Popular news  -->
                              <div class="row news-popular mt-3 p-0 m-0">
                                    @include('popular-news')
                              </div>

                              <!-- ads  -->
                              <div class="row ads-block d-sm-none d-none d-md-block mt-3 ">
                                    <div class="col-12">
                                          @include('ads-block-two')
                                    </div>
                              </div>

                        </div>
                  </div>

            </div>
            <!-- end right side  -->
      </div>
</div>
@endsection