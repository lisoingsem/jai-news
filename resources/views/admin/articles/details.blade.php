<div class="modal fade" id="viewModal" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
            <div class="modal-content">
                  <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                        </button>
                  </div>
                  <div class="modal-body">
                        <input type="text" id="eid">
                        <h2 class="title">bnm</h2>

                        <div class="more-options">
                              <span id="date">August 20, 2023</span>
                              <div>
                                    <span>comments </span>|
                                    <span>views</span>
                              </div>
                        </div>
                        <hr>

                        <p class="description" id="description">
                              
                        </p>
                  </div>
                  <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Understood</button>
                  </div>
            </div>
      </div>
</div>