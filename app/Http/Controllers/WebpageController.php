<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Article;

class WebpageController extends Controller
{
      public function index()
      {
            $data['categories'] = DB::table('categories')
                  ->where('status', '1')
                  ->get();
            return view('home', $data);
      }

      public function detail(Request $request, $name, $id)
      {
            $data['article'] = DB::table('articles')->find($id);
            $data['name'] = $name;
            $data['articles'] = DB::table('articles')
                  ->leftJoin('categories', 'categories.id', 'articles.category_id')
                  ->where('articles.status', 1)
                  ->where('categories.name', $name)
                  ->where('categories.status', 1)
                  ->where('articles.is_publish', 1)
                  ->paginate(8);
            return view('blog-detail', $data);
      }

      public function articleByCategory(Request $request, $name)
      // {
      //       $page = $request->get('page', 1);
            // $category = $name;
            // $data['articles'] = Article::where('articles.status', 1)
            //             ->join('categories', 'categories.id', 'articles.category_id')
            //             ->where('categories.name', $category)
            //             ->paginate(3, ['*'], 'page', $page);

      //       $data['name'] = $name;
      //       // return view('view-by-category', $data);


      //       // $articles = Article::paginate(5);



            // if ($request->ajax()) {

            //       $articles = view('data', $data)->render();

            //       return response()->json(['html' => $data]);
            // }
      // }
      {

            $category = $name;
            $data['articles'] = Article::where('articles.status', 1)
                        ->join('categories', 'categories.id', 'articles.category_id')
                        ->where('categories.name', $category)
                        ->paginate(3);
            $data['name'] = $name;

            if ($request->ajax()) {
    
                $view = view('data', $data)->render();
                return response()->json(['html' => $view]);
    
            }

            return view('view-by-category', $data);
    
        }
}
